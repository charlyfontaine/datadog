def mergeSort(Number):
    if len(Number)>1:
        mid = len(Number)//2
        left= Number[:mid]
        right = Number[mid:]

        #Recursive call to split the list
        mergeSort(left)
        mergeSort(right)

        #Merge part
        i=0
        j=0
        k=0

        while(i<len(left) and j<len(right)):
            if left[i]<right[j]:
                Number[k]=left[i]
                i=i+1
            else:
                Number[k] = right[j]
                j=j+1
            k=k+1
        while (i<len(left)):
            Number[k]=left[i]
            i=i+1
            k=k+1
        while (j<len(right)):
            Number[k]=right[j]
            j=j+1
            k=k+1

import random
random_items2 = [random.randint(-500, 1000) for c in range(320)]

print 'Before merge_sort: ', random_items2
mergeSort(random_items2)
print 'After merge_sort: ', random_items2


        


import random
def sort(array):
    if (len(array)>1):
        less = []
        equal = []
        greater = []

        pivot = array[0]

        for x in array:
            if x < pivot:
                less.append(x)
            if x == pivot:
                equal.append(x)
            if x > pivot:
                greater.append(x)
        return sort(less) + equal + sort(greater)
    else:
        return array


random_items3 = [random.randint(0, 10) for c in range(5)]

print 'Before quick_sort: ', random_items3

print 'After quick_sort: ', sort(random_items3)
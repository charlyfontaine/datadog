import random

def insertion_sort(items):
    """ Implementation of insertion sort """
    for i in range(1, len(items)):
        j = i
        while j > 0 and items[j] < items[j-1]:
            items[j], items[j-1] = items[j-1], items[j]
            j -= 1

random_items1 = [random.randint(-500, 1000) for c in range(320)]

print 'Before insertion_sort: ', random_items1
insertion_sort(random_items1)
print 'After insertion_sort: ', random_items1




